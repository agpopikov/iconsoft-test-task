# Тестовое задание

### Краткое описание

В репозитории реализован проект в соответствии с требованиями ТЗ.

В проекте по умолчанию настроена работа с PostgreSQL и для корректной работы `dev`-профиля
необходим запущенный экземпляр СУБД по адресу localhost:5432 с пользователем `postgres` / `postgres`.

Запустить БД можно, например, с помощью Docker, командой вида:

```shell script
docker run -d --name db -p 5432:5432 -e POSTGRES_PASSWORD=postgres postgres:12
```

### Нюансы реализации

- Актуальная JDL схема лежит в файле [app.jdl](app.jdl).
- Для реализации пункта 2 ТЗ добавлены дополнительные миграции в директории - [src/main/resources/config/liquibase/custom-changelog](src/main/resources/config/liquibase/custom-changelog).
- Пункты 4 и 5 реализованы в виде расширения функционала работы с продуктами (например "кастомный" DTO - [CustomProductDTO.java](src/main/java/org/iconsoft/test/app/service/dto/CustomProductDTO.java)).
- Пункт 6 реализован с помощью библиотеки [ShedLock](https://github.com/lukas-krecan/ShedLock), основная реализация находится в файле [OrderUpdaterTask.java](src/main/java/org/iconsoft/test/app/task/OrderUpdaterTask.java).
