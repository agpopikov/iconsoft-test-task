export interface IProductType {
  id?: number;
  code?: string;
  description?: string;
}

export class ProductType implements IProductType {
  constructor(public id?: number, public code?: string, public description?: string) {}
}
