import { IOrderItem } from 'app/shared/model/order-item.model';
import { OrderStatus } from 'app/shared/model/enumerations/order-status.model';

export interface IOrder {
  id?: number;
  status?: OrderStatus;
  items?: IOrderItem[];
  customerId?: number;
}

export class Order implements IOrder {
  constructor(public id?: number, public status?: OrderStatus, public items?: IOrderItem[], public customerId?: number) {}
}
