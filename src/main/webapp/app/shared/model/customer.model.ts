import { IOrder } from 'app/shared/model/order.model';

export interface ICustomer {
  id?: number;
  email?: string;
  fullName?: string;
  orders?: IOrder[];
}

export class Customer implements ICustomer {
  constructor(public id?: number, public email?: string, public fullName?: string, public orders?: IOrder[]) {}
}
