import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { IconsoftTestAppSharedModule } from 'app/shared/shared.module';
import { IconsoftTestAppCoreModule } from 'app/core/core.module';
import { IconsoftTestAppAppRoutingModule } from './app-routing.module';
import { IconsoftTestAppHomeModule } from './home/home.module';
import { IconsoftTestAppEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    IconsoftTestAppSharedModule,
    IconsoftTestAppCoreModule,
    IconsoftTestAppHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    IconsoftTestAppEntityModule,
    IconsoftTestAppAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class IconsoftTestAppAppModule {}
