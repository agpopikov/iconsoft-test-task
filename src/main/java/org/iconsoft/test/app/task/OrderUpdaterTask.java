package org.iconsoft.test.app.task;

import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.iconsoft.test.app.domain.enumeration.OrderStatus;
import org.iconsoft.test.app.service.OrderQueryService;
import org.iconsoft.test.app.service.OrderService;
import org.iconsoft.test.app.service.dto.OrderCriteria;
import org.iconsoft.test.app.service.dto.OrderDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OrderUpdaterTask {

    private static final Logger log = LoggerFactory.getLogger(OrderUpdaterTask.class);
    private final OrderService service;
    private final OrderQueryService queryService;

    public OrderUpdaterTask(OrderService service, OrderQueryService queryService) {
        this.service = service;
        this.queryService = queryService;
    }

    @Scheduled(cron = "0 * * * * ?")
    @SchedulerLock(name = "OrderUpdaterTask", lockAtLeastFor = "45s", lockAtMostFor = "45s")
    public void run() {
        log.info("Run OrderUpdaterTask");
        OrderCriteria criteria = new OrderCriteria();
        OrderCriteria.OrderStatusFilter filter = new OrderCriteria.OrderStatusFilter();
        filter.setEquals(OrderStatus.NEW);
        criteria.setStatus(filter);
        List<OrderDTO> newOrders = queryService.findByCriteria(criteria);
        log.trace("Found {} tasks to update", newOrders.size());
        newOrders.forEach(this::updateTask);
        log.debug("OrderUpdaterTask completed successfully");
    }

    private void updateTask(OrderDTO order) {
        try {
            order.setStatus(OrderStatus.COMPLETED);
            service.save(order);
        } catch (Exception e) {
            log.error("Failed to save task: {}, try to update in next run, error: {}", order, e.getMessage());
        }
    }
}
