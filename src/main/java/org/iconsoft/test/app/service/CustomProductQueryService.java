package org.iconsoft.test.app.service;

import org.iconsoft.test.app.domain.Product;
import org.iconsoft.test.app.domain.ProductType_;
import org.iconsoft.test.app.domain.Product_;
import org.iconsoft.test.app.repository.ProductRepository;
import org.iconsoft.test.app.service.dto.CustomProductCriteria;
import org.iconsoft.test.app.service.dto.CustomProductDTO;
import org.iconsoft.test.app.service.dto.ProductDTO;
import org.iconsoft.test.app.service.mapper.ProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Transactional(readOnly = true)
public class CustomProductQueryService extends ProductQueryService {

    private final Logger log = LoggerFactory.getLogger(CustomProductQueryService.class);

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    public CustomProductQueryService(ProductRepository productRepository, ProductMapper productMapper) {
        super(productRepository, productMapper);
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Transactional(readOnly = true)
    public List<CustomProductDTO> findCustomProductByCriteria(CustomProductCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        Specification<Product> specification = createSpecification(criteria);
        if (criteria != null && criteria.getTypeCode() != null) {
            specification = specification.and(buildSpecification(criteria.getTypeCode(),
                root -> root.join(Product_.type, JoinType.LEFT).get(ProductType_.code)));
        }
        return productRepository.findAll(specification).stream().map(i -> {
            ProductDTO productDTO = productMapper.toDto(i);
            String code = i.getType() != null ? i.getType().getCode() : null;
            return new CustomProductDTO(productDTO, code);
        }).collect(Collectors.toList());
    }
}
