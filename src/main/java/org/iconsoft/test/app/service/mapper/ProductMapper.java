package org.iconsoft.test.app.service.mapper;


import org.iconsoft.test.app.domain.*;
import org.iconsoft.test.app.service.dto.ProductDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProductTypeMapper.class})
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {

    @Mapping(source = "type.id", target = "typeId")
    ProductDTO toDto(Product product);

    @Mapping(source = "typeId", target = "type")
    Product toEntity(ProductDTO productDTO);

    default Product fromId(Long id) {
        if (id == null) {
            return null;
        }
        Product product = new Product();
        product.setId(id);
        return product;
    }
}
