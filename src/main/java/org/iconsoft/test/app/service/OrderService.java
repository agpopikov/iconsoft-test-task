package org.iconsoft.test.app.service;

import org.iconsoft.test.app.service.dto.OrderDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link org.iconsoft.test.app.domain.Order}.
 */
public interface OrderService {

    /**
     * Save a order.
     *
     * @param orderDTO the entity to save.
     * @return the persisted entity.
     */
    OrderDTO save(OrderDTO orderDTO);

    /**
     * Get all the orders.
     *
     * @return the list of entities.
     */
    List<OrderDTO> findAll();


    /**
     * Get the "id" order.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrderDTO> findOne(Long id);

    /**
     * Delete the "id" order.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
