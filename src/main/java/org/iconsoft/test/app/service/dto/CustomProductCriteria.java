package org.iconsoft.test.app.service.dto;

import io.github.jhipster.service.filter.StringFilter;

import java.util.Objects;

public class CustomProductCriteria extends ProductCriteria {

    private static final long serialVersionUID = 1L;

    private StringFilter typeCode;

    public CustomProductCriteria() {
    }

    public CustomProductCriteria(CustomProductCriteria other) {
        super(other);
        this.typeCode = other.typeCode == null ? null : other.typeCode.copy();
    }

    @Override
    public CustomProductCriteria copy() {
        return new CustomProductCriteria(this);
    }

    public StringFilter getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(StringFilter typeCode) {
        this.typeCode = typeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CustomProductCriteria that = (CustomProductCriteria) o;
        return Objects.equals(typeCode, that.typeCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), typeCode);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CustomProductCriteria{" +
            "typeCode=" + typeCode +
            "} " + super.toString();
    }
}
