package org.iconsoft.test.app.service;

import org.iconsoft.test.app.service.dto.ProductTypeDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link org.iconsoft.test.app.domain.ProductType}.
 */
public interface ProductTypeService {

    /**
     * Save a productType.
     *
     * @param productTypeDTO the entity to save.
     * @return the persisted entity.
     */
    ProductTypeDTO save(ProductTypeDTO productTypeDTO);

    /**
     * Get all the productTypes.
     *
     * @return the list of entities.
     */
    List<ProductTypeDTO> findAll();


    /**
     * Get the "id" productType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProductTypeDTO> findOne(Long id);

    /**
     * Delete the "id" productType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
