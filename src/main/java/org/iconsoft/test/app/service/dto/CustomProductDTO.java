package org.iconsoft.test.app.service.dto;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

public class CustomProductDTO implements Serializable {

    @NotNull
    @JsonUnwrapped
    private ProductDTO product;

    private String typeCode;

    public CustomProductDTO() {
        // stub
    }

    public CustomProductDTO(@NotNull ProductDTO product, @NotNull String typeCode) {
        this.product = product;
        this.typeCode = typeCode;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomProductDTO that = (CustomProductDTO) o;
        return Objects.equals(product, that.product) &&
            Objects.equals(typeCode, that.typeCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, typeCode);
    }

    @Override
    public String toString() {
        return "CustomProductDTO{" +
            "product=" + product +
            ", typeCode='" + typeCode + '\'' +
            '}';
    }
}
