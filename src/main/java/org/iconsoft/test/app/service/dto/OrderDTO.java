package org.iconsoft.test.app.service.dto;

import java.io.Serializable;
import org.iconsoft.test.app.domain.enumeration.OrderStatus;

/**
 * A DTO for the {@link org.iconsoft.test.app.domain.Order} entity.
 */
public class OrderDTO implements Serializable {
    
    private Long id;

    private OrderStatus status;


    private Long customerId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderDTO)) {
            return false;
        }

        return id != null && id.equals(((OrderDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderDTO{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", customerId=" + getCustomerId() +
            "}";
    }
}
