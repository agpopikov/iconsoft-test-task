package org.iconsoft.test.app.web.rest;

import org.iconsoft.test.app.service.CustomProductQueryService;
import org.iconsoft.test.app.service.dto.CustomProductCriteria;
import org.iconsoft.test.app.service.dto.CustomProductDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CustomProductResource {

    private static final Logger log = LoggerFactory.getLogger(CustomProductResource.class);

    private final CustomProductQueryService service;

    public CustomProductResource(CustomProductQueryService service) {
        this.service = service;
    }

    @GetMapping("/products/custom")
    public ResponseEntity<List<CustomProductDTO>> getCustomProducts(CustomProductCriteria criteria) {
        log.debug("REST request to get custom products by criteria: {}", criteria);
        List<CustomProductDTO> result = service.findCustomProductByCriteria(criteria);
        return ResponseEntity.ok().body(result);
    }
}
