/**
 * View Models used by Spring MVC REST controllers.
 */
package org.iconsoft.test.app.web.rest.vm;
