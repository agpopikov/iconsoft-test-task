package org.iconsoft.test.app.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    NEW, COMPLETED, CANCELLED
}
